# [Amethyst SMP](https://charaka-janith.web.app) [![Tweet](https://img.shields.io/twitter/url/http/shields.io.svg?style=social&logo=twitter)](https://twitter.com/CHARAKAJANITH1)

![version](https://img.shields.io/badge/version-1.0.0-blue.svg) ![license](https://img.shields.io/badge/license-MIT-blue.svg) [![GitHub issues open](https://img.shields.io/github/issues/creativetimofficial/blk-design-system-react.svg?maxAge=2592000)](https://gitlab.com/charaka_janith/amethyst-smp/-/issues) [![GitHub issues closed](https://img.shields.io/github/issues-closed-raw/creativetimofficial/blk-design-system-react.svg?maxAge=2592000)](https://gitlab.com/charaka_janith/amethyst-smp/-/issues) [![Join the chat at https://gitter.im/NIT-dgp/General](https://badges.gitter.im/NIT-dgp/General.svg)](https://gitter.im/charaka-janith/Lobby) [![Chat](https://img.shields.io/badge/chat-on%20discord-7289da.svg)](https://discord.gg/)

![Product Presentation Image](https://charaka-janith.web.app/static/media/Chamiya_A.S.K_rivendell__elves__4k_69f348de-4b08-4cdf-a46d-2b31d21106c4.7ce1ae94.png?raw=true)

**[Amethyst SMP](https://gitlab.com/charaka_janith/amethyst-smp)** is a bla bla bla, developed using [React](https://reactjs.org/), [Reactstrap](https://reactstrap.github.io/) and [create-react-app](https://facebook.github.io/create-react-app/), and it is provided for free by Charaka Janith. It is a beautiful cross-platform UI kit bla bla bla.

Amethyst SMP• Design System React will help you bla bla bla.

## Complex Documentation

bla bla bla presented in a very complex documentation. You can read more about the idea behind this design system here. You can check the bla bla bla here.

## Bootstrap 4 Support

bla bla bla• Design System React is built on top of the much awaited Bootstrap 4 (Reactstrap). This makes starting a new project very simple. It also provides benefits if you are already working on a Bootstrap 4 or Reactstrap project; you can just import the bla bla bla• Design System React style over it. Most of the elements have been redesigned; but if you are using an element we have not touched, it will fall back to the Bootstrap default.

## Table of Contents

- [Versions](#versions)
- [PRO Versions](#pro-versions)
- [Demo](#demo)
- [Quick Start](#quick-start)
- [Documentation](#documentation)
- [File Structure](#file-structure)
- [Browser Support](#browser-support)
- [Resources](#resources)
- [Reporting Issues](#reporting-issues)
- [Licensing](#licensing)
- [Useful Links](#useful-links)

## Versions

| 2023.01.01 |
|------------|
| 1.0.0      |

## Quick start

- `npm i`
- Install with [Bower](https://bower.io/): `bower install`.
- Clone the repo: `git clone https://gitlab.com/charaka_janith/amethyst-smp.git`.

## Documentation

The documentation for the Amethyst SMP is hosted at our [website](https://charaka-janith.web.app/).

## File Structure

Within the structure you'll find the following directories and files:

```
Blk• Design System React
.
├── CHANGELOG.md
├── ISSUE_TEMPLATE.md
├── README.md
├── package.json
├── public
│   ├── favicon.ico
│   ├── index.html
│   └── manifest.json
└── src
    ├── index.js
    ├── variables
    │   └── charts.js
    ├── assets
    │   ├── css
    │   │   ├── blk-design-system-react.css
    │   │   ├── blk-design-system-react.css.map
    │   │   ├── blk-design-system-react.min.css
    │   │   └── nucleo-icons.css
    │   ├── demo
    │   │   └── demo.css
    │   ├── fonts
    │   │   ├── nucleo.eot
    │   │   ├── nucleo.ttf
    │   │   ├── nucleo.woff
    │   │   └── nucleo.woff2
    │   ├── img
    │   └── scss
    │       ├── blk-design-system-react
    │       │   ├── bootstrap
    │       │   │   ├── mixins
    │       │   │   └── utilities
    │       │   ├── custom
    │       │   │   ├── cards
    │       │   │   ├── mixins
    │       │   │   ├── sections
    │       │   │   ├── utilities
    │       │   │   └── vendor
    │       │   └── react
    │       │       └── react-differences.scss
    │       └── blk-design-system-react.scss
    ├── components
    │   ├── Footer
    │   │   └── Footer.js
    │   ├── Navbars
    │   │   ├── ComponentsNavbar.js
    │   │   └── ExamplesNavbar.js
    │   └── PageHeader
    │       └── PageHeader.js
    └── views
        ├── Index.js
        ├── IndexSections
        │   ├── Basics.js
        │   ├── Download.js
        │   ├── Examples.js
        │   ├── JavaScript.js
        │   ├── Navbars.js
        │   ├── Notifications.js
        │   ├── NucleoIcons.js
        │   ├── Pagination.js
        │   ├── Signup.js
        │   ├── Tabs.js
        │   └── Typography.js
        └── examples
            ├── LandingPage.js
            ├── ProfilePage.js
            └── RegisterPage.js
```

## Browser Support

At present, we officially aim to support the last two versions of the following browsers:

|                                                              Chrome                                                              |                                                              Firefox                                                               |                                                              Edge                                                               |                                                              Safari                                                               |                                                              Opera                                                               |
| :------------------------------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------------------------------------------------------------------------------: | :-----------------------------------------------------------------------------------------------------------------------------: | :-------------------------------------------------------------------------------------------------------------------------------: | :------------------------------------------------------------------------------------------------------------------------------: |
| <img src="https://github.com/creativetimofficial/public-assets/blob/main/logos/chrome-logo.png?raw=true" width="64" height="64"> | <img src="https://raw.githubusercontent.com/creativetimofficial/public-assets/main/logos/firefox-logo.png" width="64" height="64"> | <img src="https://raw.githubusercontent.com/creativetimofficial/public-assets/main/logos/edge-logo.png" width="64" height="64"> | <img src="https://raw.githubusercontent.com/creativetimofficial/public-assets/main/logos/safari-logo.png" width="64" height="64"> | <img src="https://raw.githubusercontent.com/creativetimofficial/public-assets/main/logos/opera-logo.png" width="64" height="64"> |

## Reporting Issues

We use GitLab Issues as the official bug tracker for the System. Here are some advices for our users that want to report an issue:

1. Make sure that you are using the latest version of the System. Check the CHANGELOG from your dashboard on our [website](https://charaka-janith.web.app/).
2. Providing us reproducible steps for the issue will shorten the time it takes for it to be fixed. Some issues may be browser specific, so specifying in what browser you encountered the issue might help.

## Licensing

- Copyright 2023 Charaka Janith (https://charaka-janith.web.app/)

- Licensed under MIT (https://gitlab.com/charaka_janith/amethyst-smp/blob/main/LICENSE.md)

## Useful Links

- [Creator](https://gitlab.com/charaka_janith/amethyst-smp)
