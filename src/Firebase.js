// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyDGA6bpcbeW40xHIUTdLcmSxn9HOpcfg_s",
    authDomain: "amethyst-smp.firebaseapp.com",
    projectId: "amethyst-smp",
    storageBucket: "amethyst-smp.appspot.com",
    messagingSenderId: "1090999797659",
    appId: "1:1090999797659:web:64c5bc583a82b12dbf833e",
    measurementId: "G-F8MXDPFD3X"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);