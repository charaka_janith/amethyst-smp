import React from "react";
import "./PageHeader.css";

// reactstrap components
import {Button, Container} from "reactstrap";

export default function PageHeader() {
  return (
    <div className="page-header header-filter">
      <div className="squares square1" />
      <div className="squares square2" />
      <div className="squares square3" />
      <div className="squares square4" />
      <div className="squares square5" />
      <div className="squares square6" />
      <div className="squares square7" />
      <Container>
        <div className="content-center brand">
          <h1 className="h1-seo">Amethyst• SMP</h1>
          <h3 className="d-none d-sm-block">
            Official web page of the Amethyst Community
          </h3>
            <Button
                className="btn-simple btn-round"
                color="neutral"
                href="https://discord.gg/nvQRBzC2Ga"
            >
                Join Our Discord Server&nbsp;&nbsp;
                <i aria-hidden={true} className="fab fa-discord" />
            </Button>
        </div>
      </Container>
    </div>
  );
}
