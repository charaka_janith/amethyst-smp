import React from "react";
import classnames from "classnames";
// react plugin used to create datetimepicker

// reactstrap components
import {
  Button,
  FormGroup,
  Container,
  Row,
  Col,
  UncontrolledTooltip,
  UncontrolledPopover,
  PopoverBody,
  PopoverHeader,
  Modal,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Label,
  UncontrolledCarousel
} from "reactstrap";

const carouselItems = [
  {
    src: require("assets/img/denys.jpg"),
    altText: "Slide 1",
    caption: ""
  },
  {
    src: require("assets/img/fabien-bazanegue.jpg"),
    altText: "Slide 2",
    caption: ""
  },
  {
    src: require("assets/img/mark-finn.jpg"),
    altText: "Slide 3",
    caption: ""
  }
];

export default function JavaScript() {
  return (
    <div className="section section-javascript" id="javascriptComponents">
      {/*<img alt="..." className="path" src={require("assets/img/path5.png")} />*/}
      <img
        alt="..."
        className="path path1"
        src={require("assets/img/path5.png")}
      />
      <div className="section">
        <Container>
          <Row className="justify-content-between align-items-center">
            <Col lg="6">
              <UncontrolledCarousel
                  items={carouselItems}
                  indicators={false}
                  autoPlay={false}
              />
            </Col>
            <Col className="mb-5 mb-lg-0" lg="5">
              <h1 className="text-white font-weight-light">
                A <span className="text-info">FUN</span> filled Adventure
              </h1>
              <p className="text-white mt-4">
                Prepare for and adventure of limitless possibilities as you build, mine, battle mobs, and explore the ever-changing Minecraft landscape. Come join us today!
              </p>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}
