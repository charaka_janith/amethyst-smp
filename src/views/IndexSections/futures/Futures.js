import React from "react";
import classnames from "classnames";
import { Link } from "react-router-dom";
import "./Futures.css";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardImg,
  CardTitle,
  Label,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
  Col
} from "reactstrap";

export default function Signup() {
  return (
    <div className="section section-signup">
      <Container>
        <div className="squares square-4" >
          <img
              alt="..."
              className="img-fluid"
              src={require("assets/img/landing/creeper.png")}
              style={{ width: "500px" }}
          />
        </div>
        <Row className="row-grid justify-content-between align-items-center">
          <Col className="col-lg-8">
            <p className="text-info mb-3">
              Amethyst SMP Futures
            </p>
            <h3 className="display-3 text-white">
              We're not just any Minecraft server
            </h3>
            <div className="typography-line">
              <h2>
                <span><i className="icon tim-icons icon-triangle-right-17 text-info" /></span>
                OneBlock
              </h2>
            </div>
            <p className="mb-5">
              OneBlock is a survival map in which you stand on a lonely block floating in the void. You can mine the same block over and over, and it gives you basic materials that slowly become better and better. You go through certain phases, and the infinite block slowly upgrades to better blocks, chests and new mobs!
            </p>
            <div className="typography-line">
              <h2>
                <span><i className="icon tim-icons icon-triangle-right-17 text-info" /></span>
                Survival & Slimefun
              </h2>
            </div>
            <p className="mb-5">
              Survival mode is one of the main game modes in Minecraft. Slimefun is a plugin which adds a modpack experience like FTB without installing a single mod.
              New items and blocks to explore!
            </p>
            <div className="typography-line">
              <h2>
                <span><i className="icon tim-icons icon-triangle-right-17 text-info" /></span>
                Minigames
              </h2>
            </div>
            <p>
              Wanting to have fun? Come and enjoy all types of Minigames on the server!
              Arcade, Skywars, Bedwars, Practice and Weekly UHC
            </p>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
